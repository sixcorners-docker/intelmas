#syntax=docker/dockerfile:1
FROM ubuntu
RUN apt-get update \
 && apt-get install -y curl unzip smartmontools nvme-cli \
 && rm -rf /var/lib/apt/lists/*
RUN mkdir -p /opt/intelmas \
 && cd /opt/intelmas \
 && curl -sO https://downloadmirror.intel.com/763590/Intel_MAS_CLI_Tool_Linux_2.2.zip \
 && unzip *.zip \
 && dpkg -i intelmas_*_amd64.deb \
 && rm *.deb *.rpm *.zip
ENTRYPOINT ["intelmas"]
